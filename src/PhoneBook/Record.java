package PhoneBook;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by user on 01.11.2016.
 */
public class Record implements Serializable {
    transient private String firstName;
    transient private String secondName;
    transient private String surname;
    private Long number;
    private String fullName;
    private String phoneNumber;
    private String address;

    public void setAddress(String address) {this.address = address; }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSurname() {
        return surname;
    }

    public Long getNumber() {
        return number;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }
    public void createFullName() {
        StringBuilder sb = new StringBuilder();
        if (surname != null) sb.append(surname).append(" ");
        if (firstName != null) sb.append(firstName).append(" ");
        if (secondName != null) sb.append(secondName).append(" ");
        fullName = sb.toString();
    }

    private void setFields(){
        String[] fields = this.fullName.split(" ");
        this.surname = fields[0];
        this.firstName = fields[1];
        this.secondName = fields[2];

    }
    private void writeObject(ObjectOutputStream out) throws IOException {
        if (fullName == null) createFullName();
        out.defaultWriteObject();
    }
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
        in.defaultReadObject();
        setFields();
    }

    @Override
    public String toString() {
        return "Record{ number=" + number +
                ", fullName='" + fullName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (fullName != null ? !fullName.equals(record.fullName) : record.fullName != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(record.phoneNumber) : record.phoneNumber != null) return false;
        return address != null ? address.equals(record.address) : record.address == null;

    }

    @Override
    public int hashCode() {
        int result = fullName != null ? fullName.hashCode() : 0;
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

}
