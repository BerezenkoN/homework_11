package PhoneBook;

import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by user on 01.11.2016.
 */
public class PhoneBook implements Serializable {
    private List <Record> records = new ArrayList<>();

    public void addRecord(Record record){
    record.setNumber((long) records.size() + 1);
    records.add(record);
    }
    public void removeRecord(Record record){
        records.remove(record);
        for(Record temp : records){
            if(temp.getNumber()>record.getNumber()){
                temp.setNumber(temp.getNumber()-1);

            }
        }
    }
    public boolean contains (Record record){
        return records.contains(record);
    }
    public Record findByNumber(Long number){
        for(Record record : records){
            if(record.getNumber().equals(number))
                return record;
        }
        return null;
    }
    public List<Record> findByFullName(String name){
        List<Record> findRecords =new ArrayList<>();
        if(name.isEmpty()){
            return findRecords;
        }
        else{
            for(Record record : records){
                if(record.getFullName().contains(name.trim())){
                    findRecords.add(record);
                }
            }
        }
        return findRecords;
    }
    public List<Record> findByPhoneNumber(String phoneNumber){
        List<Record> findRecords = new ArrayList<>();
        if(phoneNumber.isEmpty()) {
            return findRecords;
        }
        else{
            for(Record record : records){
                if(record.getPhoneNumber().equals(phoneNumber.trim())){
                    findRecords.add(record);
                }
            }
        }
        return findRecords;
    }
    public List<Record> findByAddress(String address){
        List<Record> findRecords = new ArrayList<>();
        if(address.isEmpty()){
            return findRecords;
        }
        else{
            for(Record record : records){
                if(record.getAddress().contains(address.trim())){
                    findRecords.add(record);
                }
            }
        }
        return findRecords;

    }

    @Override
    public String toString() {
        return "PhoneBook{" +
                "records=" + records +
                '}';
    }
}
