package PhoneBook;

import java.io.*;
import java.util.List;

/**
 * Created by user on 01.11.2016.
 */
public class Main  {
    static PhoneBook phoneBook;
    public static void writeToFile()throws IOException, ClassNotFoundException{
        FileOutputStream fos = new FileOutputStream("phoneBook.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(phoneBook);
        oos.close();

    }
    public static PhoneBook readFromFile() throws IOException, ClassNotFoundException{
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("phoneBook.txt"));
        PhoneBook phoneBook = (PhoneBook) ois.readObject();
        ois.close();
        return phoneBook;
    }
    public static void createRecord(BufferedReader reader)throws IOException{

        if(phoneBook ==null) phoneBook = new PhoneBook();

        System.out.println("Creating new record:");

        Record record = new Record();

        System.out.print("Enter first name:");

        record.setFirstName(reader.readLine());

        System.out.print("Enter second name:");

        record.setSecondName(reader.readLine());

        System.out.print("Enter surname:");

        record.setSurname(reader.readLine());

        System.out.print("Enter phone number:");

        record.setPhoneNumber(reader.readLine());

        System.out.print("Enter address:");

        record.setAddress(reader.readLine());

        record.createFullName();

        if (phoneBook.contains(record)) {

            System.out.println("--- Such record already exists! ---");
        }

        else {

            phoneBook.addRecord(record);
        }

    }
    public static void updateRecord(BufferedReader reader) throws IOException {

        System.out.print("Update record with number:");
        String number = reader.readLine();
        Record record = phoneBook.findByNumber(Long.parseLong(number));
        if(record == null){
            System.out.println("--- No record with such number! ---");
        }
        else {
            String newValue;
            System.out.println("Enter new surname: ");
            newValue = reader.readLine();
            if (!newValue.equals("")) record.setSurname(newValue);
            System.out.println("Enter new first name: ");
            newValue = reader.readLine();
            if (!newValue.equals("")) record.setFirstName(newValue);
            System.out.println("Enter new second name: ");
            newValue = reader.readLine();
            if (!newValue.equals("")) record.setSecondName(newValue);
            System.out.println("Enter new phone number name: ");
            newValue = reader.readLine();
            if (!newValue.equals("")) record.setPhoneNumber(newValue);
            System.out.println("Enter new address name: ");
            newValue = reader.readLine();
            if (!newValue.equals("")) record.setAddress(newValue);
            record.createFullName();
        }

    }
    public static void removeRecord(BufferedReader reader) throws IOException {
        String number = reader.readLine();
        Record record = phoneBook.findByNumber(Long.parseLong(number));
        if(record == null){
            System.out.println("--- No record with such number! ---");
        }
        else {
            phoneBook.removeRecord(record);
        }
    }
    public static void findRecord(BufferedReader reader) throws IOException {
        String parametrSearch = reader.readLine();
        List<Record> records;
    switch (parametrSearch) {
        case "1":
            System.out.print("Enter full name:");
            records = phoneBook.findByFullName(reader.readLine());
            printRecords(records);
            saveToFile(reader, records);
            break;
        case "2":
            System.out.print("Enter phone number:");
            records = phoneBook.findByPhoneNumber(reader.readLine());
            printRecords(records);
            saveToFile(reader, records);
            break;
        case "3":
            System.out.print("Enter address:");
            records = phoneBook.findByAddress(reader.readLine());
            printRecords(records);
            saveToFile(reader, records);
            break;
        default:
            System.out.println("Wrong parametr");
        }
    }

    public static void saveToFile(BufferedReader reader, List<Record> records)throws IOException{
        System.out.println("Do you want save search result (Yes/No)?");
        switch (reader.readLine()) {
            case "Yes":
                BufferedWriter fileWriter = new BufferedWriter(new FileWriter("searchResult.txt"));
                for (Record record : records) {
                    fileWriter.write(record.toString());
                    fileWriter.newLine();
                }
                fileWriter.close();
                break;
            case "No":
                break;
        }
    }
        private static void printRecords (List<Record> records){
            if(records.size() == 0) {
                System.out.println("--- Records not found ---");
            }
            else{
                System.out.println("Search results:");
                for(Record record : records)

                System.out.println(record);

            }

        }
       public static void main(String [] args)throws IOException, ClassNotFoundException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Start of the program");
        String s;
        boolean exit = false;
        while(!exit){
            System.out.println("Enter the command: ");
            s = reader.readLine();
            switch (s) {
                case "print":
                    System.out.println(phoneBook);
                    break;
                case "add":
                    System.out.println("Creating new record:");
                    createRecord(reader);
                    break;
                case "update":
                    System.out.println("Update record with number:");
                    updateRecord(reader);
                    break;
                case "remove":
                    System.out.println("Remove record with number:");
                    removeRecord(reader);
                    break;
                case "find":
                    System.out.println("Find record by (1. Name, 2. PhoneNumber, 3. Address):");
                    findRecord(reader);
                    break;
                case "exit":
                    writeToFile();
                    reader.close();
                    exit = true;
                    break;
                default:
                    System.out.println("Wrong command!");
            }
        }
    }


}
